### Remember to read our contribution guidelines:

* [Formatting Guidelines](https://develop.kde.org/docs/contribute/formatting/)
* [Style Guidelines](https://develop.kde.org/docs/contribute/style/)

### What does this merge request add?

(Mention **what your merge request does** and **the parts of the website that it changes**)

### Related Issues

(Link the **issues related to this merge request** here)

### Screenshot of the result (optional)

(If this merge request introduces a visual change or a new website feature, please **add a screenshot** by using the clip button in the upper right of this editor, otherwise leave this section blank)

### How to test this MR

```bash
git clone https://invent.kde.org/documentation/develop-kde-org.git
cd develop-kde-org
git mr <THE NUMBER OF THIS MR>
hugo server
```

### Closes

(If this merge request closes any issue in this repository, use `Closes #<mr number>` here, for example: `Closes #3`)
